import { Provider } from "react-redux";
import { store } from "./src/store";
import Router from "./src/routes/Router";
import * as eva from "@eva-design/eva";
import { ApplicationProvider } from "react-native-ui-kitten";
import RefreshTokenProvider from "./src/components/providers/RefreshTokenProvider";
import Toast from "react-native-toast-message";
import ProvideAttachment from "./src/components/providers/ProvideAttachment";
import "./src/i18n/i18n";

export default function App() {
  return (
    <ApplicationProvider {...eva} theme={eva.light}>
      <Provider store={store}>
        <RefreshTokenProvider>
          <ProvideAttachment>
            <Router />
          </ProvideAttachment>
        </RefreshTokenProvider>
      </Provider>
      <Toast />
    </ApplicationProvider>
  );
}
