import React from "react";
import Container from "../ui/Container";
import { Text } from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { generalColor } from "../../styles/general";
import { useTranslation } from "react-i18next";

const NoAttachments = () => {
  const { t } = useTranslation("attachment");

  return (
    <Container flex={1} direction="column" gap={20} justifyContent="center" alignItems="center">
      <MaterialCommunityIcons name="clipboard-text-off" size={100} color={generalColor} />
      <Text style={{ fontWeight: "500", fontSize: 24 }}>{t("no_attachment")}</Text>
    </Container>
  );
};

export default NoAttachments;
