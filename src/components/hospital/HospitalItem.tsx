import React, { FC } from "react";
import { IHospital } from "../../types/hospitals";
import Container from "../ui/Container";
import { hospitalsStyles } from "../../styles/hospitalsStyles";
import { Text } from "@ui-kitten/components";
import { useTranslation } from "react-i18next";
import i18n from "../../i18n/i18n";

interface HospitalItemIProps {
  hospital: IHospital;
  fullId?: boolean;
}

const HospitalItem: FC<HospitalItemIProps> = ({ fullId, hospital }) => {
  const { t } = useTranslation("hospitals");

  return (
    <Container flex={1} gap={10} style={hospitalsStyles.hospitalItem} direction="column">
      <Container flex={1} gap={10} direction="row">
        <Text style={{ fontWeight: "700" }}>ID:</Text>
        <Text>{fullId ? hospital.id : hospital.id.slice(0, 15) + "..."}</Text>
      </Container>
      <Container flex={1} direction="row" gap={10} alignItems="center" justifyContent="space-between">
        <Text style={hospitalsStyles.hospitalName}>{i18n.language === "ru" ? hospital.nameRU : hospital.nameEN}</Text>
        <Text>{hospital.address}</Text>
      </Container>
      {hospital.isPrivate && <Text style={{ fontWeight: "700" }}>{t("private")}</Text>}
    </Container>
  );
};

export default HospitalItem;
