import React, { FC, memo } from "react";
import { FlatList, View } from "react-native";
import { ScrollView } from "react-native";
import { IPaginatedList } from "../../types/paginations";
import { IHospital } from "../../types/hospitals";
import Container from "../ui/Container";
import { Text } from "react-native-ui-kitten";
import HospitalItem from "./HospitalItem";
import { TouchableOpacity } from "react-native-gesture-handler";

interface HospitalListIProps {
  data?: IPaginatedList<IHospital>;
  goToHospital: (id: string) => void;
}

const HospitalList: FC<HospitalListIProps> = ({ data, goToHospital }) => {
  return (
    <View style={{ width: "100%", height: "82%" }}>
      {data && data.rows.length > 0 && (
        <FlatList
          data={data.rows}
          keyExtractor={info => info.id}
          renderItem={info => {
            return (
              <TouchableOpacity onPress={() => goToHospital(info.item.id)}>
                <HospitalItem hospital={info.item} />
              </TouchableOpacity>
            );
          }}
        />
      )}
    </View>
  );
};

export default memo(HospitalList);
