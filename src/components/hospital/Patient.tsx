import React from "react";
import Container from "../ui/Container";
import { Text } from "react-native";
import { useAppSelector } from "../../hooks/redux";
import { patientStyles } from "../../styles/patientStyles";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

const Patient = () => {
  const user = useAppSelector(state => state.userReducer.user);

  return (
    <Container flex={1} gap={8} style={patientStyles.patientContainer} alignItems="center" direction="row">
      <MaterialCommunityIcons name="account" size={50} />
      <Container gap={8} direction="column">
        <Text style={patientStyles.patientName}>
          {user?.firstName} {user?.lastName} {user?.secondName}
        </Text>
        <Text style={{ fontSize: 18, fontWeight: "700" }}>{user?.iin}</Text>
      </Container>
    </Container>
  );
};

export default Patient;
