import RNPickerSelect from "react-native-picker-select";
import i18n from "../../i18n/i18n";
import { changeLanguage } from "../../utils/changeLanguage";
import { useTranslation } from "react-i18next";

const LangSelector = () => {
  const { t } = useTranslation("profile");
  const languages = [
    { label: "Русский", value: "ru" },
    { label: "English", value: "en" },
  ];

  return (
    <RNPickerSelect
      style={{ viewContainer: { width: 120 } }}
      placeholder={{ label: t("change_language") }}
      value={i18n.language}
      onValueChange={value => changeLanguage(value)}
      items={languages.map(language => language)}
    />
  );
};

export default LangSelector;
