import React, { FC } from "react";
import Container from "../ui/Container";
import { Text } from "react-native";

export interface LabelWithValueIProps {
  label: string;
  value: string;
}

const LabelWithValue: FC<LabelWithValueIProps> = ({ value, label }) => {
  return (
    <Container direction="row" gap={10}>
      <Text style={{ fontWeight: "700" }}>{label}:</Text>
      <Text>{value}</Text>
    </Container>
  );
};

export default LabelWithValue;
