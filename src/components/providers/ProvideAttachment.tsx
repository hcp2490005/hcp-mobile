import { ReactNode, useEffect } from "react";
import { useAppSelector } from "../../hooks/redux";
import { useGetOwnAttachmentQuery } from "../../store/api/hospitalsApi";

export default function ProvideAttachment({ children }: { children: ReactNode }) {
  const userId = useAppSelector(state => state.userReducer.user?.id);
  const { data: attachmentData, refetch } = useGetOwnAttachmentQuery(userId);

  useEffect(() => {
    refetch();
  }, []);

  return children;
}
