import { FC, PropsWithChildren, useEffect } from "react";
import { useRefreshQuery } from "../../store/api/tokenApi";
import { Text } from "react-native";
import Container from "../ui/Container";
import LoadingContainer from "../ui/LoadingContainer";
import { setDefaultLanguage } from "../../utils/setLanguage";

const RefreshTokenProvider: FC<PropsWithChildren> = ({ children }) => {
  const { data, isLoading } = useRefreshQuery(null);

  useEffect(() => {
    setDefaultLanguage();
  }, []);
  if (isLoading) {
    return <LoadingContainer />;
  }

  return children;
};

export default RefreshTokenProvider;
