import { FC, PropsWithChildren } from "react";
import * as eva from "@eva-design/eva";
import { ApplicationProvider } from "@ui-kitten/components";
const UIProvider: FC<PropsWithChildren> = ({ children }) => {
  return (
    <ApplicationProvider {...eva} theme={eva.light}>
      {children}
    </ApplicationProvider>
  );
};

export default UIProvider;
