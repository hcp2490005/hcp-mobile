import React from "react";
import { Text, StyleSheet, Pressable, DimensionValue } from "react-native";
import { generalColor } from "../../styles/general";

interface ButtonIProps {
  onPress(): void;
  title: string;
  mH?: number;
  mV?: number;
  disabled?: boolean;
  width?: number | DimensionValue;
}

export default function Button({ width, disabled, mH, mV, onPress, title }: ButtonIProps) {
  const disabledStyles = disabled ? { ...styles.button_disabled } : {};

  return (
    <Pressable
      disabled={disabled}
      style={{ ...styles.button, width, marginHorizontal: mH, marginVertical: mV, ...disabledStyles }}
      onPress={onPress}
    >
      <Text style={styles.text}>{title}</Text>
    </Pressable>
  );
}

const styles = StyleSheet.create({
  button: {
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 12,
    paddingHorizontal: 32,
    borderRadius: 4,
    elevation: 3,
    backgroundColor: generalColor,
  },
  button_disabled: {
    opacity: 0.6,
  },
  text: {
    fontSize: 14,
    lineHeight: 21,
    fontWeight: "bold",
    letterSpacing: 0.25,
    color: "white",
  },
});
