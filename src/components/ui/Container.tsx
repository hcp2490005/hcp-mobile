import { FC, ReactNode } from "react";
import { StyleSheet, View, ViewStyle } from "react-native";

interface ContainerIProps {
  children?: ReactNode;
  direction: "row" | "column";
  justifyContent?: ViewStyle["justifyContent"];
  alignItems?: ViewStyle["alignItems"];
  gap?: ViewStyle["gap"];
  padding?: ViewStyle["padding"];
  style?: ViewStyle;
  flex?: ViewStyle["flex"];
}

const Container: FC<ContainerIProps> = props => {
  const { children } = props;
  return <View style={containerStyles(props).container}>{children}</View>;
};

const containerStyles = (styles: ContainerIProps) =>
  StyleSheet.create({
    container: {
      ...styles.style,
      padding: styles.padding,
      justifyContent: styles.justifyContent || "flex-start",
      alignItems: styles.alignItems || "flex-start",
      flex: styles.flex,
      gap: styles.gap || 0,
      display: "flex",
      flexDirection: styles.direction,
    },
  });

export default Container;
