import { FC } from "react";
import { TextInput, TextInputProps } from "react-native";

interface InputIProps extends TextInputProps {
  variant?: null;
}

const Input: FC<InputIProps> = ({ style, onChangeText, placeholder, id, placeholderTextColor, value, onChange }) => {
  return (
    <TextInput
      style={style}
      placeholder={placeholder}
      id={id}
      placeholderTextColor={placeholderTextColor}
      value={value}
      onChangeText={onChangeText}
    />
  );
};

export default Input;
