import React from "react";
import Container from "./Container";
import { Text } from "react-native";
import { useTranslation } from "react-i18next";

const LoadingContainer = () => {
  const { t } = useTranslation("profile");

  return (
    <Container flex={1} justifyContent="center" alignItems="center" direction="row">
      <Text style={{ fontSize: 32 }}>{t("loading_label")}</Text>
    </Container>
  );
};

export default LoadingContainer;
