export default {
  title: "Your attachment",
  id: "ID of attachment",
  date_of_creation: "Date of creation",
  status: "Status of attachment",
  hospital_name: "Hospital name",
  address: "Address of hospital",
  attachment_date: "Date of response",
  answered: "Answered",
  no_answer: "No answer",
  unbind: "Unbind",
  no_attachment: "You don't have an attachment",
};
