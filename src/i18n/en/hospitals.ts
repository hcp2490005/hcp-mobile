export default {
  attachment_created: "Attachment created",
  location: "Location",
  attach: "Attach",
  private: "Private",
  search_placeholder: "Name of hospital",
  search_btn: "Search",
  nothing_found: "Nothing found matching your request",
};
