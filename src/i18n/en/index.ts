import attachment from "./attachment";
import hospitals from "./hospitals";
import login from "./login";
import profile from "./profile";
import tabs from "./tabs";

export const en = { profile, tabs, attachment, login, hospitals };
