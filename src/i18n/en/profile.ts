export default {
  IIN: "IIN",
  first_name: "First name",
  second_name: "Last name",
  last_name: "Patronymic",
  address: "Address",
  change_language: "Choose language",
  loading_label: "Loading...",
};
