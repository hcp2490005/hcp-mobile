export default {
  profile: "Profile",
  hospitals: "Hospitals",
  attachments: "Attachments",
  logout: "Logout",
  single_hospital: "Hospital",
  login: "Login",
};
