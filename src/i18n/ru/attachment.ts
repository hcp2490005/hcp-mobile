export default {
  title: "Ваше прикрепление",
  id: "ID прикрепления",
  date_of_creation: "Дата создания",
  status: "Статус прикрепления",
  hospital_name: "Поликлиника",
  address: "Адрес полклиники",
  attachment_date: "Дата ответа",
  answered: "Ответил (-а)",
  no_answer: "Нет ответа",
  unbind: "Открепиться",
  no_attachment: "У Вас нет прикрепления",
};
