export default {
  attachment_created: "Прикрепление создано",
  location: "Местоположение",
  attach: "Прикрепиться",
  private: "Частная",
  search_placeholder: "Название поликлиники",
  search_btn: "Найти",
  nothing_found: "Ничего не найдено по Вашему запросу",
};
