export default {
  IIN: "ИИН",
  first_name: "Имя",
  second_name: "Фамилия",
  last_name: "Отчество",
  address: "Адрес",
  change_language: "Выберите язык",
  loading_label: "Загрузка...",
};
