export default {
  profile: "Профиль",
  hospitals: "Поликлиники",
  attachments: "Прикрепления",
  logout: "Выйти",
  single_hospital: "Поликлиника",
  login: "Войти",
};
