import React, { FC, useEffect, useMemo, useState } from "react";
import { ScrollView, Text, View } from "react-native";
import { AttachmentProps } from "../types/router";
import { useAppSelector } from "../hooks/redux";
import { useGetOwnAttachmentQuery, useUnbindFromHospitalMutation } from "../store/api/hospitalsApi";
import Container from "../components/ui/Container";
import NoAttachments from "../components/attachment/NoAttachments";
import { attachmentStyles } from "../styles/attachmentStyles";
import LabelWithValue, { LabelWithValueIProps } from "../components/profile/LabelWithValue";
import { StatusesEnum } from "../types/statuses";
import { getDate } from "../utils/getDate";
import { useTranslation } from "react-i18next";
import i18n from "../i18n/i18n";
import Button from "../components/ui/Button";
import { RefreshControl } from "react-native-gesture-handler";

const Attachment: FC<AttachmentProps> = () => {
  const userId = useAppSelector(state => state.userReducer.user?.id);
  const { data, isLoading: loadingAttachment, refetch } = useGetOwnAttachmentQuery(userId);
  const [unbind, { isLoading }] = useUnbindFromHospitalMutation();
  const { t } = useTranslation("attachment");
  const [refresh, setRefresh] = useState<boolean>(false);

  const unbindHandler = () => {
    unbind();
  };

  const onRefresh = () => {
    setRefresh(true);
    setTimeout(() => {
      refetch();
      setRefresh(false);
    }, 1000);
  };

  const attachmentData: LabelWithValueIProps[] = useMemo(
    () =>
      data && data.attachment !== null
        ? [
            { value: data.attachment.id.slice(0, 20) + "...", label: t("id") },
            { value: getDate(data.attachment.createdDate), label: t("date_of_creation") },
            { value: data.attachment.statusName, label: t("status") },
            {
              value: data.attachment.hospital[("name" + i18n.language.toUpperCase()) as "nameRU" | "nameEN"],
              label: t("hospital_name"),
            },
            { value: data.attachment.hospital.address, label: t("address") },
            {
              value: data.attachment.attachmentDate ? getDate(data.attachment.attachmentDate) : t("no_answer"),
              label: t("attachment_date"),
            },
          ]
        : [],
    [data, i18n.language],
  );

  return (
    <ScrollView
      contentContainerStyle={{ height: "100%", width: "100%" }}
      refreshControl={<RefreshControl refreshing={refresh} onRefresh={onRefresh} />}
    >
      {data?.attachment ? (
        <>
          <Text style={{ marginHorizontal: 20, fontSize: 18, marginTop: 20 }}>{t("title")}</Text>
          <Container
            direction="column"
            gap={8}
            justifyContent="flex-start"
            style={attachmentStyles.attachmentContainer}
          >
            {attachmentData.map(data => (
              <LabelWithValue value={data.value} label={data.label} key={data.label} />
            ))}
          </Container>
          {data.attachment.stuff && (
            <Container direction="column" style={attachmentStyles.attachmentContainer}>
              <LabelWithValue
                value={
                  data.attachment.stuff.firstName +
                  " " +
                  data.attachment.stuff.lastName +
                  " " +
                  data.attachment.stuff.secondName
                }
                label={`${t("answered")}`}
              />
            </Container>
          )}
          {data.attachment.statusName === StatusesEnum.SUCCESS && (
            <Button mH={20} title={t("unbind")} onPress={unbindHandler} />
          )}
        </>
      ) : (
        <NoAttachments />
      )}
    </ScrollView>
  );
};

export default Attachment;
