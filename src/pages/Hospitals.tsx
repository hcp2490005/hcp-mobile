import React, { ChangeEvent, FC, useState } from "react";
import { HospitalsProps } from "../types/router";
import { useGetHospitalsListQuery } from "../store/api/hospitalsApi";
import Container from "../components/ui/Container";
import { Input } from "@ui-kitten/components";
import UIProvider from "../components/providers/UIProvider";
import { hospitalsStyles } from "../styles/hospitalsStyles";
import HospitalList from "../components/hospital/HospitalList";
import { Text } from "react-native";
import Pagination from "@cherry-soft/react-native-basic-pagination";
import { generalColor } from "../styles/general";
import { useTranslation } from "react-i18next";
import Button from "../components/ui/Button";
const Hospitals: FC<HospitalsProps> = ({ navigation }) => {
  const [search, setSearch] = useState<string>("");
  const [reqSearch, setReqSearch] = useState<string>("");
  const [page, setPage] = useState<number>(1);

  const { data, isLoading, refetch } = useGetHospitalsListQuery({ limit: 8, search: reqSearch, page });
  const { t } = useTranslation("hospitals");

  const goToHospital = (id: string) => {
    navigation.navigate("SingleHospital", { hospitalId: id });
  };

  const searchHandler = () => {
    setReqSearch(search);
    refetch();
    setPage(1);
  };

  const onChangeSearchHandler = (val: string) => {
    setSearch(val);
  };
  return (
    <UIProvider>
      <Container flex={1} padding={10} direction="column" justifyContent="flex-start">
        <Container style={{ zIndex: 2, marginBottom: 10 }} justifyContent="flex-start" gap={10} direction="row">
          <Input
            value={search}
            onChangeText={onChangeSearchHandler}
            textStyle={{ paddingBottom: 6 }}
            style={hospitalsStyles.input}
            placeholder={t("search_placeholder")}
          />
          <Button title={t("search_btn")} onPress={searchHandler} />
        </Container>
        {data && data.rows.length === 0 ? (
          <Container direction="row" justifyContent="center">
            <Text>{t("nothing_found")}</Text>
          </Container>
        ) : (
          <HospitalList goToHospital={goToHospital} data={data} />
        )}
        {data && data.rows.length ? (
          <Container direction="row" style={{ width: "100%" }} justifyContent="center">
            <Pagination
              btnStyle={hospitalsStyles.paginationButton}
              activeBtnStyle={hospitalsStyles.paginationActiveButton}
              totalItems={data.count}
              onPageChange={setPage}
              currentPage={page}
              pageSize={8}
              pagesToDisplay={5}
            />
          </Container>
        ) : (
          <></>
        )}
      </Container>
    </UIProvider>
  );
};

export default Hospitals;
