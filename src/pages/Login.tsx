import { FC, useState } from "react";
import { LoginProps } from "../types/router";
import Container from "../components/ui/Container";
import Input from "../components/ui/Input";
import { Text } from "react-native-ui-kitten";
import { loginStyles } from "../styles/loginStyles";
import { useLoginMutation } from "../store/api/tokenApi";
import { useTranslation } from "react-i18next";
import Button from "../components/ui/Button";

const Login: FC<LoginProps> = ({ navigation }) => {
  const [iin, setIin] = useState<string>("");
  const [submit, { isLoading, error, isError }] = useLoginMutation();
  const { t } = useTranslation("login");

  const submitHandler = () => {
    submit({ email: iin, password: "" });
  };

  return (
    <Container flex={1} gap={10} justifyContent="center" alignItems="center" direction="column">
      <Input
        style={loginStyles.loginInput}
        placeholder={t("iin_placeholder")}
        value={iin}
        onChangeText={val => setIin(val)}
      />
      <Button title={t("login")} width={"70%"} onPress={submitHandler} disabled={isLoading} />
      {isError && <Text style={loginStyles.errorText}>{(error as { data: { message: string } }).data.message}</Text>}
    </Container>
  );
};

export default Login;
