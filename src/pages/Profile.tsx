import { FC } from "react";
import { ProfileProps } from "../types/router";
import Container from "../components/ui/Container";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { useAppSelector } from "../hooks/redux";
import { profileStyles } from "../styles/profileStyles";
import LabelWithValue from "../components/profile/LabelWithValue";
import { generalColor } from "../styles/general";
import { useTranslation } from "react-i18next";

const Profile: FC<ProfileProps> = () => {
  const { t } = useTranslation("profile");
  const user = useAppSelector(state => state.userReducer.user);
  const userProps = user
    ? [
        { label: t("IIN"), value: user.iin },
        { label: t("first_name"), value: user.firstName },
        { label: t("second_name"), value: user.lastName },
        { label: t("last_name"), value: user.secondName },
        { label: t("address"), value: user.address },
      ]
    : [];
  return (
    <Container
      flex={1}
      style={profileStyles.profileContainer}
      direction="column"
      alignItems="center"
      justifyContent="center"
    >
      <MaterialCommunityIcons name="account" color={generalColor} size={150} />
      <Container
        flex={1}
        alignItems="flex-start"
        justifyContent="flex-start"
        gap={20}
        style={profileStyles.infoContainer}
        direction="column"
      >
        {user && userProps.map(prop => <LabelWithValue key={prop.label} value={prop.value} label={prop.label} />)}
      </Container>
    </Container>
  );
};

export default Profile;
