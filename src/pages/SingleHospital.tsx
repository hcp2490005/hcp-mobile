import { FC, useEffect } from "react";
import Toast from "react-native-toast-message";
import { SingleHospitalProps } from "../types/router";
import { useGetHospitalQuery, useGetOwnAttachmentQuery, usePostRequestByIdMutation } from "../store/api/hospitalsApi";
import LoadingContainer from "../components/ui/LoadingContainer";
import Container from "../components/ui/Container";
import HospitalItem from "../components/hospital/HospitalItem";
import UIProvider from "../components/providers/UIProvider";
import Patient from "../components/hospital/Patient";
import MapView, { Marker } from "react-native-maps";
import { hospitalsStyles } from "../styles/hospitalsStyles";
import { useAppSelector } from "../hooks/redux";
import { StatusesEnum } from "../types/statuses";
import { Text } from "react-native";
import { useTranslation } from "react-i18next";
import i18n from "../i18n/i18n";
import Button from "../components/ui/Button";

const SingleHospital: FC<SingleHospitalProps> = ({ navigation, route }) => {
  const id = route.params?.hospitalId;
  const userId = useAppSelector(state => state.userReducer.user?.id);
  const [postRequest, { isLoading: loadingRequest }] = usePostRequestByIdMutation();
  const { data: attachmentData, isLoading: loadingAttachment } = useGetOwnAttachmentQuery(userId);
  const { t } = useTranslation("hospitals");

  const { data, isLoading } = useGetHospitalQuery(id);

  if (isLoading || !data) {
    return <LoadingContainer />;
  }

  const postRequestHandler = () => {
    postRequest(data.id)
      .unwrap()
      .then(() => {
        Toast.show({
          type: "info",
          text1: t("attachment_created"),
        });
        navigation.navigate("Hospitals");
      });
  };

  return (
    <UIProvider>
      <Container style={{ marginHorizontal: 10, width: "95%" }} flex={0.7} justifyContent="center" direction="column">
        <Patient />
        <HospitalItem fullId hospital={data} />
      </Container>
      <Container style={{ marginHorizontal: 20, marginVertical: 10 }} direction="column">
        <Text style={{ fontSize: 18 }}>{t("location")}:</Text>
      </Container>
      <MapView
        style={{ marginHorizontal: 20, width: "90%", height: 360 }}
        region={{
          latitude: data.lat,
          longitude: data.lon,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        }}
      >
        <Marker
          coordinate={{ latitude: data.lat, longitude: data.lon }}
          title={data[("name" + i18n.language.toUpperCase()) as "nameRU" | "nameEN"]}
          description={data.address}
        />
      </MapView>
      {(attachmentData?.attachment === null || attachmentData?.attachment.statusName === StatusesEnum.REJECTED) && (
        <Container flex={1} direction="row" style={hospitalsStyles.buttonContainer}>
          <Button title={t("attach")} onPress={postRequestHandler} disabled={isLoading} mV={10} width={"90%"} />
        </Container>
      )}
    </UIProvider>
  );
};

export default SingleHospital;
