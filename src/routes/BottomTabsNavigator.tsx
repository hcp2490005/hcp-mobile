import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { NestedRouterProps, RootStackParamList } from "../types/router";
import Profile from "../pages/Profile";
import Hospitals from "../pages/Hospitals";
import Attachment from "../pages/Attachment";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { TouchableOpacity } from "react-native";
import { Text } from "react-native";
import { useLogoutMutation } from "../store/api/tokenApi";
import LangSelector from "../components/localization/LangSelector";
import { useTranslation } from "react-i18next";

const Tab = createBottomTabNavigator<RootStackParamList>();

export default function BottomTabs({ navigation }: NestedRouterProps) {
  const [logout, { isLoading }] = useLogoutMutation();
  const { t } = useTranslation("tabs");
  return (
    <Tab.Navigator
      initialRouteName="Hospitals"
      screenOptions={{
        tabBarActiveTintColor: "#0066CC",
        headerLeftContainerStyle: { justifyContent: "center", display: "flex", flex: 1 },
        headerLeft: () => <LangSelector />,
        headerRight: () => (
          <TouchableOpacity
            disabled={isLoading}
            onPress={() => {
              logout(null)
                .unwrap()
                .catch(err => console.log(err));
            }}
          >
            <Text style={{ color: "#000", fontSize: 16, marginRight: 15 }}>{t("logout")}</Text>
          </TouchableOpacity>
        ),
      }}
    >
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          headerTitle: t("profile"),
          tabBarLabel: t("profile"),
          tabBarIcon: ({ color, size }) => <MaterialCommunityIcons name="account" color={color} size={size} />,
        }}
      />
      <Tab.Screen
        name="Hospitals"
        component={Hospitals}
        options={{
          headerTitle: t("hospitals"),
          tabBarLabel: t("hospitals"),
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="hospital-building" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Attachment"
        component={Attachment}
        options={{
          headerTitle: t("attachments"),
          tabBarLabel: t("attachments"),
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="book-open-outline" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}
