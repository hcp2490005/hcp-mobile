import { createStackNavigator } from "@react-navigation/stack";
import { FC } from "react";
import { useAppSelector } from "../hooks/redux";
import Login from "../pages/Login";
import { RootStackParamList } from "../types/router";
import { NavigationContainer } from "@react-navigation/native";
import BottomTabs from "./BottomTabsNavigator";
import SingleHospital from "../pages/SingleHospital";
import ProvideAttachment from "../components/providers/ProvideAttachment";
import { useTranslation } from "react-i18next";
import LangSelector from "../components/localization/LangSelector";

const RootStack = createStackNavigator<RootStackParamList>();

const Router: FC = () => {
  const isAuth = useAppSelector(state => state.userReducer.isAuth);
  const { t } = useTranslation("tabs");

  return (
    <NavigationContainer>
      <RootStack.Navigator initialRouteName={isAuth ? "NestedRouter" : "Login"}>
        {!isAuth && (
          <RootStack.Screen
            name="Login"
            options={{ headerTitle: t("login"), headerRight: () => <LangSelector /> }}
            component={Login}
          />
        )}
        {isAuth && <RootStack.Screen name="NestedRouter" options={{ headerShown: false }} component={BottomTabs} />}
        {isAuth && (
          <RootStack.Screen
            name="SingleHospital"
            options={{ headerTitle: t("single_hospital") }}
            component={SingleHospital}
          />
        )}
      </RootStack.Navigator>
    </NavigationContainer>
  );
};

export default Router;
