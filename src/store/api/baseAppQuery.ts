import { fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { injectToken } from "./injectToken";
import i18n from "../../i18n/i18n";

export const BASE_APP_URL = `${process.env.EXPO_PUBLIC_API_URL}/api/`;

const baseAppQuery = fetchBaseQuery({
  credentials: "include",
  baseUrl: BASE_APP_URL,
  prepareHeaders: async (headers, api) => {
    headers = await injectToken(headers, api);
    headers.set("Content-Language", "ru");
    headers.set("Content-Language", i18n.language);
    return headers;
  },
});

export default baseAppQuery;
