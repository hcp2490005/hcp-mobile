import { createApi } from "@reduxjs/toolkit/query/react";
import { IHospital, IQueryParams } from "../../types/hospitals";
import baseReauthQuery from "./baseReauthQuery";
import { IPaginatedList } from "../../types/paginations";
import { IAttachment } from "../../types/patients";
import { RootState } from "..";

const ENDPOINT = "/hospital/";

export const hospitalsApi = createApi({
  reducerPath: "hospitalsApi",
  baseQuery: baseReauthQuery,
  tagTypes: ["hospitals", "attachment"],
  endpoints: builder => ({
    getHospital: builder.query<IHospital, string>({
      query: id => `${ENDPOINT}/hospitals/${id}`,
      providesTags: [{ type: "hospitals", id: "LIST" }],
    }),
    getHospitalsList: builder.query<IPaginatedList<IHospital>, IQueryParams>({
      query: qParams => {
        const params: IQueryParams = {};
        if (qParams.is_private) {
          params["is_private"] = qParams.is_private;
        }
        params["limit"] = qParams.limit ?? 10;
        params["page"] = qParams.page ?? 1;
        params["search"] = qParams.search ?? "";

        return { url: `${ENDPOINT}/hospitals`, params };
      },
    }),
    postRequestById: builder.mutation<void, string>({
      query: id => ({
        url: "/patient/create-attachment",
        method: "POST",
        body: { hospitalId: id },
      }),
      invalidatesTags: ["attachment"],
    }),
    getOwnAttachment: builder.query<{ attachment: IAttachment | null }, string | undefined>({
      query: () => ({
        url: "/patient/get-own-attachment",
      }),
      providesTags: ["attachment"],
    }),
    unbindFromHospital: builder.mutation<void, void>({
      query: () => ({
        url: "/patient/unlink-from-hospital",
        method: "DELETE",
      }),
      invalidatesTags: ["attachment"],
    }),
  }),
});

export const attachmentSelector = (userId: string | undefined) => (state: RootState) =>
  hospitalsApi.endpoints.getOwnAttachment.select(userId)(state);

export const {
  useUnbindFromHospitalMutation,
  useGetOwnAttachmentQuery,
  useGetHospitalsListQuery,
  useGetHospitalQuery,
  usePostRequestByIdMutation,
} = hospitalsApi;
