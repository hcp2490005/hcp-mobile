import { BaseQueryApi } from "@reduxjs/toolkit/query";
import AsyncStorage from "@react-native-async-storage/async-storage";

export async function injectToken(
  headers: Headers,
  api: Pick<BaseQueryApi, "getState" | "extra" | "endpoint" | "type" | "forced">
): Promise<Headers> {
  const accessToken = await AsyncStorage.getItem("accessToken");
  const refreshToken = await AsyncStorage.getItem("refreshToken");
  if (accessToken) {
    headers.set("Authorization", `Bearer ${accessToken}`);
  }
  if (refreshToken) {
    headers.set("Refresh-Token", refreshToken);
  }
  return headers;
}
