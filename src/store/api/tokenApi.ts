import { createApi } from "@reduxjs/toolkit/query/react";

import { ILoginData, IToken } from "../../types/token";
import baseAppQuery from "./baseAppQuery";

export const tokenApi = createApi({
  reducerPath: "tokenApi",
  baseQuery: baseAppQuery,
  endpoints: builder => ({
    login: builder.mutation<IToken, ILoginData>({
      query: (credentials: ILoginData) => {
        const formData = {
          iin: credentials.email,
        };
        return {
          url: "/auth/patient/login",
          method: "POST",
          body: formData,
        };
      },
    }),
    logout: builder.mutation<unknown, unknown>({
      query: () => {
        return {
          method: "GET",
          url: "/auth/logout",
        };
      },
    }),
    refresh: builder.query<IToken, unknown>({
      query: () => {
        return {
          url: "/auth/refresh",
        };
      },
    }),
  }),
});

export const { useLoginMutation, useLogoutMutation, useRefreshQuery } = tokenApi;
export default tokenApi;
