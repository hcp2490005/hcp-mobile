import { configureStore, combineReducers } from "@reduxjs/toolkit";
import tokenApi from "./api/tokenApi";
import { authReducer } from "./slice/userSlice";
import { hospitalsApi } from "./api/hospitalsApi";

const ownReducer = combineReducers({
  [tokenApi.reducerPath]: tokenApi.reducer,
  [hospitalsApi.reducerPath]: hospitalsApi.reducer,
  userReducer: authReducer,
});

export const store = configureStore({
  reducer: ownReducer,
  middleware: getDefaultMiddlewares => getDefaultMiddlewares().concat([tokenApi.middleware, hospitalsApi.middleware]),
});

export type RootState = ReturnType<typeof ownReducer>;
export type AppStore = typeof store;
export type AppDispatch = AppStore["dispatch"];
