import { createSlice } from "@reduxjs/toolkit";

import { IToken } from "../../types/token";
import tokenApi from "../api/tokenApi";
import { IAuthState } from "../../types/user";
import AsyncStorage from "@react-native-async-storage/async-storage";

const initialState: IAuthState = {
  user: null,
  isAuth: false,
};

const clearTokenStateReducer = (state: IAuthState) => {
  AsyncStorage.removeItem("accessToken");
  AsyncStorage.removeItem("refreshToken");
  state.user = null;
  state.isAuth = false;
};

const writeTokenReducer = (state: IAuthState, { payload }: { payload: IToken }) => {
  AsyncStorage.setItem("accessToken", payload.tokenData.accessToken);
  AsyncStorage.setItem("refreshToken", payload.tokenData.refreshToken);
  state.user = payload.user;
  state.isAuth = true;
};

export const userSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    clearTokenState: clearTokenStateReducer,
    writeToken: writeTokenReducer,
  },
  extraReducers: builder => {
    builder
      .addMatcher(tokenApi.endpoints.login.matchFulfilled, writeTokenReducer)
      .addMatcher(tokenApi.endpoints.refresh.matchFulfilled, writeTokenReducer)
      .addMatcher(tokenApi.endpoints.logout.matchFulfilled, clearTokenStateReducer);
  },
});

const authReducer = userSlice.reducer;
export const { clearTokenState, writeToken } = userSlice.actions;

export { authReducer };
