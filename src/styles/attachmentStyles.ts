import { StyleSheet } from "react-native";

export const attachmentStyles = StyleSheet.create({
  attachmentContainer: {
    backgroundColor: "#FFF",
    margin: 20,
    paddingVertical: 20,
    paddingHorizontal: 20,
    borderRadius: 8,
  },
});
