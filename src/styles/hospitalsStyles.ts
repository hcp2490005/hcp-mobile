import { StyleSheet } from "react-native";
import { generalColor } from "./general";

export const hospitalsStyles = StyleSheet.create({
  input: { flex: 1 },
  hospitalItem: {
    backgroundColor: "white",
    borderRadius: 8,
    paddingVertical: 20,
    paddingHorizontal: 20,
    width: "100%",
    marginBottom: 10,
  },
  hospitalName: { fontWeight: "600", fontSize: 18 },
  buttonContainer: { width: "100%", height: "auto", maxHeight: "10%", marginHorizontal: 20 },
  paginationButton: { backgroundColor: generalColor, borderWidth: 0, borderRadius: 30 },
  paginationActiveButton: { backgroundColor: generalColor, opacity: 0.6 },
});
