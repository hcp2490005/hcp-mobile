import { StyleSheet } from "react-native";

export const loginStyles = StyleSheet.create({
  loginInput: {
    height: 80,
    width: "70%",
    borderRadius: 8,
    paddingHorizontal: 20,
    backgroundColor: "white",
    fontSize: 24,
  },
  button: {
    width: "70%",
  },
  errorText: {
    color: "red",
  },
});
