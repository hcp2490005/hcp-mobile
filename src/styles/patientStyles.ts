import { StyleSheet } from "react-native";

export const patientStyles = StyleSheet.create({
  patientContainer: {
    paddingVertical: 20,
    paddingHorizontal: 20,
    height: "20%",
    maxHeight: "40%",
  },
  patientName: { fontSize: 18, fontWeight: "700", color: "#0066CC" },
});
