import { StyleSheet } from "react-native";

export const profileStyles = StyleSheet.create({
  profileContainer: {
    marginTop: 40,
  },
  infoContainer: {
    backgroundColor: "#FFF",
    borderRadius: 8,
    marginBottom: 100,
    paddingVertical: 50,
    paddingLeft: 60,
    width: "70%",
  },
});
