import { IPerson } from "./person";

export interface IHospital {
  id: string;
  nameEN: string;
  nameRU: string;
  lat: number;
  lon: number;
  address: string;
  isPrivate: boolean;
  createdDate: string;
  updatedDate: string;
  stuff: Array<IPerson>;
}

export interface IQueryParams {
  limit?: number;
  page?: number;
  search?: string;
  is_private?: "true" | "false";
}
