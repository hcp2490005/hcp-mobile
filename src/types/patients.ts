import { IHospital } from "./hospitals";
import { IPerson } from "./person";
import { StatusesEnum } from "./statuses";

export interface IAttachment extends Pick<IHospital, "id" | "createdDate" | "updatedDate"> {
  patientId: string;
  statusName: StatusesEnum;
  hospitalName: string;
  hospitalId: string;
  stuffId: string | null;
  attachmentDate: string | null;
  hospital: IHospital;
  stuff: IPerson;
}
