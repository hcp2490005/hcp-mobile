type ROLES = "MANAGER" | "ADMIN";

export interface IPerson {
  id: string;
  roleName: ROLES;
  firstName: string;
  secondName: string;
  lastName: string;
  email: string;
  hospitalId: string;
}
