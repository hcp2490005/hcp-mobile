import { NativeStackScreenProps } from "react-native-screens/lib/typescript/native-stack/types";

export type RootStackParamList = {
  Login: undefined;
  Profile: undefined;
  Hospitals: undefined;
  Attachment: undefined;
  NestedRouter: undefined;
  SingleHospital: { hospitalId: string };
};

export type SingleHospitalProps = NativeStackScreenProps<RootStackParamList, "SingleHospital">;
export type LoginProps = NativeStackScreenProps<RootStackParamList, "Login">;
export type ProfileProps = NativeStackScreenProps<RootStackParamList, "Profile">;
export type HospitalsProps = NativeStackScreenProps<RootStackParamList, "Hospitals">;
export type AttachmentProps = NativeStackScreenProps<RootStackParamList, "Attachment">;
export type NestedRouterProps = NativeStackScreenProps<RootStackParamList, "NestedRouter">;
