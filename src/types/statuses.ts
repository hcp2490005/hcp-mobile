export enum StatusesEnum {
  PROCESSING = "PROCESSING",
  SUCCESS = "SUCCESS",
  REJECTED = "REJECTED",
}
