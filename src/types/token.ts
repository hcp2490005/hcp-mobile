export interface ILoginData {
  email: string;
  password: string;
}

export interface IToken {
  tokenData: ITokenData;
  user: ITokenUserData;
}

export interface ITokenData {
  accessToken: string;
  refreshToken: string;
}

export interface ITokenUserData {
  address: string;
  firstName: string;
  hospitalId: string | null;
  id: string;
  iin: string;
  lastName: string;
  secondName: string;
  roleName: string;
}
