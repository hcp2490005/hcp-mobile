import { ITokenUserData } from "./token";

export interface IAuthState {
  user: ITokenUserData | null;
  isAuth: boolean;
}
