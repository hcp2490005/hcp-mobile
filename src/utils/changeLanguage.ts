import AsyncStorage from "@react-native-async-storage/async-storage";
import i18n from "../i18n/i18n";

export const changeLanguage = (lang: string) => {
  i18n.changeLanguage(lang);
  AsyncStorage.setItem("i18n", lang);
};
