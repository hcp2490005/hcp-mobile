export const getDate = (date: string) => {
  return new Date(date).toLocaleString("ru-RU", {
    day: "numeric",
    month: "numeric",
    year: "numeric",
    minute: "numeric",
    hour: "numeric",
  });
};
