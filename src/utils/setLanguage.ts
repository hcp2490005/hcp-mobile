import AsyncStorage from "@react-native-async-storage/async-storage";
import i18n from "../i18n/i18n";

export const setDefaultLanguage = async () => {
  const language = await AsyncStorage.getItem("i18n");
  if (language) {
    i18n.changeLanguage(language);
  }
};
